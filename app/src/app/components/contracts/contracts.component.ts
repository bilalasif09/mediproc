import {Component, ElementRef, ViewChild} from '@angular/core';
import {LoginService} from '../../services/login.services'
import {TenderService} from '../../services/tender.service'
import {VendorService} from '../../services/vendor.service'
import {ConfigsService} from '../../services/configs.service'
import {ContractTemplateService} from '../../services/contracttemplate.service'
import {VendorContractService} from '../../services/vendorcontract.service'
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { Tender } from '../../models/tender';
import * as moment from "moment";
import { ModalDirective } from 'ngx-bootstrap/modal/modal.component';
import { GlobalEventsManager } from "../../services/eventsmanager.service";

@Component({
    selector: 'contracts',
    providers: [TenderService, 
                ConfigsService, 
                VendorService, 
                VendorContractService, 
                ContractTemplateService],
    templateUrl: 'contracts.html',
    styleUrls: ['contracts.css']
})

export class contracts {
    
    allVendors = [];
    selectedVendor: any;
    showVendorFlag: boolean = false;
    
    allTenders = [];
    selectedTender: any;

    allContractTemplates = [];
    showContractFlag: boolean = false;
    viewContractFlag: boolean = false;
    selectedTemplate: any;
    contractTemplate: any = "";
    contractInfo: any = null;

    allContracts = []
    existFlag: boolean = false;
    viewContract: any;

    constructor(private _router: Router, 
                private _loginService: LoginService,
                private _formBuilder: FormBuilder,
                private _service: ConfigsService,
                private _tenderService: TenderService,
                private _contractTemplateService: ContractTemplateService,
                private _vendorService: VendorService,
                private _vendorContractService: VendorContractService,                
                private _eventMangaerService: GlobalEventsManager){
      
      this._eventMangaerService.showNavBar(true);
    }

    ngOnInit(){
      // this._tender
      this._tenderService.getAllTenders()
        .subscribe(tenders => {
            if (!tenders[0].status){
                this._router.navigate(['blank']);
            }
            this.allTenders = tenders[0].tenders;
            console.log("ALL TENDERS: ", this.allTenders);
      });      
      this._vendorService.getAllVendors()
        .subscribe(vendors => {
            if (!vendors[0].status){
                this._router.navigate(['blank']);
            }
            this.allVendors = vendors[0].vendors;
            console.log("ALL Vendors: ", this.allVendors);
      });
      this._contractTemplateService.getAllContrcatTemplates()
          .subscribe(result => {
            if (!result[0].status){
              this.allContractTemplates = [];
            }
            this.allContractTemplates = result[0].results
            console.log("ALL Contact templates: ", this.allContractTemplates);
      });
      this.getAllVendorContracts(); 
    }
    getAllVendorContracts(){
      this._vendorContractService.getAllVendorContracts()
        .subscribe(contracts => {
          console.log("ALL vendor Contacts from query: ", contracts);
          if (!contracts[0].status){
            this.allContracts = [];
          }
          this.allContracts = contracts[0].contracts;
          console.log("ALL vendor Contacts: ", this.allContracts);

      });
    }
    selectVendor(tenderId){
      console.log("Tender To Contract: ", tenderId);
      for (let tender of this.allTenders){
        if (tenderId === tender._id){
          this.selectedTender = tender;
        }
      }
      for (let i=0; i<this.allVendors.length; i++){
        for(let j=0; j<this.allContracts.length; j++){
          if (this.allVendors[i]._id === this.allContracts[j].vendorId
              && tenderId === this.allContracts[j].tenderId){
            console.log("Matched Entry:---", this.allVendors[i].name);
            this.allVendors[i].status = "preview";
            console.log("Matched Entry:---", this.allVendors[i].status);
            break
          }else{
            this.allVendors[i].status = "pending";
            console.log("Do not match:---", this.allVendors[i].status);
          }
        }
      }
      console.log("Selected Tender: ", this.selectedTender);
      console.log("All Vendors After loop: ", this.allVendors);
      this.showVendorFlag = true;
    }

    hideVendor(){
      this.showVendorFlag = false;
      // this.selectedTender = null;
    }
    selectContract(vendorId){
      console.log("Vendor To Contract: ", vendorId);
      for (let vendor of this.allVendors){
        if (vendorId === vendor._id){
          this.selectedVendor = vendor;
        }
      }
      var tenderId = this.selectedTender._id;
      for (let j=0; j<this.allContracts.length; j++){
        if ((this.allContracts[j].vendorId == vendorId) && (this.allContracts[j].tenderId == tenderId)){
            console.log("VALUE EXISTS++++++ ", this.existFlag);
            console.log("Matched contract++++++ ", this.allContracts[j]);
            this.viewContract = this.allContracts[j];
            this.existFlag = true;
        }
      }
      console.log("Selected Tender: ", this.selectedTender);
      console.log("Selected Vendor: ", this.selectedVendor);
      this.showContractFlag = true;
    }
    generateTemplate($event){
      this.contractTemplate = $event;
      console.log("Selected contract: ", this.contractTemplate);
      this._contractTemplateService.getSignleTemplate(this.contractTemplate)
          .subscribe(result => {
            if (!result[0].status){
              this.selectedTemplate = null;
            }  
            this.selectedTemplate = result[0].result;
            console.log("Selected Template: ", this.selectedTemplate);
            this.contractInfo = this.selectedTemplate.profiletext;
            this.contractInfo = this.contractInfo.toString().replace(/vendor/g, this.selectedVendor.name);
          });
    }
    hideContractFlag(){
      this.selectedVendor = "";
      this.contractInfo = "";
      this.contractTemplate = "";
      this.viewContract = "";
      this.showContractFlag = false;
      this.viewContractFlag = false;
      this.existFlag = false;
    }

    addVendorContract(){
        var date = moment().format();
        console.log("SAVING THE VENDOR CONTRACT WITH TEH IFORMATION: ",
                    "VendorId: ", this.selectedVendor._id,
                    "TenderId: ", this.selectedTender._id,
                    "ProfileId: ", this.selectedTemplate._id,
                    "ProfileText: ", this.contractInfo,
                    "contractdate: ", date);
        this._vendorContractService.addVendorContract(this.selectedVendor._id,
                                                      this.selectedTender._id,
                                                      this.selectedTemplate._id,
                                                      this.contractInfo,
                                                      date)
            .subscribe(result => {
                console.log("results from add vednor contract.", result[0].result);
            });
    }

    viewVendorContract(){
      this.viewContractFlag = true;
    }    
    hideVendorContract(){
      this.viewContractFlag = false;
    }
}