import { Component, ElementRef, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormControl, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import * as moment from "moment";
import {Http, Headers, RequestOptions} from '@angular/http';
import { GlobalEventsManager } from "../../../services/eventsmanager.service";


@Component({
    selector: 'pco-home',
    providers: [],
    templateUrl: 'pcohome.html',
    styleUrls: ['pcohome.css']
})

export class pcohome {
  constructor(private _router: Router,
              private http: Http, 
              private _formBuilder: FormBuilder,
              private _eventMangaerService: GlobalEventsManager){
    this._eventMangaerService.showNavBar(true);
  }
}