import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import * as moment from "moment";


@Injectable()
export class DemandService{
    userId: any;
    
    constructor(private http: Http, 
                private _router: Router){
      console.log('demand Service Initialized...');
      this.userId = localStorage.getItem("userid");
    }
    getAllDemands(){
      return this.http.get("http://localhost:8008/api/alldemands/",{"params":{
                            "userId": this.userId } })
        .map(res => res.json());
    }
    updateTenderDemandStatus(tenderId,demState){
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http.post("http://localhost:8008/api/updatetenderdemandstate",{
                                      "userId": this.userId,
                                      "tenderId": tenderId,
                                      "demState": demState
                                    }, options)
                   .map(res => res.json());
    }
    updateDemandStatus(demandId,demState){
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http.post("http://localhost:8008/api/updatedemandstate",{
                                      "userId": this.userId,
                                      "demandId": demandId,
                                      "demState": demState
                                    }, options)
                   .map(res => res.json());
    }
    updateDistrictDemandStatus(tenderId,deptId,district,demState){
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http.post("http://localhost:8008/api/updatedistrictdemandstate",{
                                      "userId": this.userId,
                                      "tenderId": tenderId,
                                      "deptId": deptId,
                                      "district": district,
                                      "demState": demState
                                    }, options)
                   .map(res => res.json());
    }
    updateDeptDemandStatus(tenderId,deptId,demState){
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http.post("http://localhost:8008/api/updatedeptdemandstate",{
                                      "userId": this.userId,
                                      "tenderId": tenderId,
                                      "deptId": deptId,
                                      "demState": demState
                                    }, options)
                   .map(res => res.json());
    }
    getDDDemands(departmentId,tenderId,district){
      console.log("Depat id",departmentId);
      return this.http.get("http://localhost:8008/api/dddemands/",{"params":{
                            "userId": this.userId, "departmentId": departmentId, "tenderId": tenderId,
                            "district": district } })
        .map(res => res.json());
    }
    getDepartmentDemands(departmentId,tenderId){
      console.log("Depat id",departmentId);
      return this.http.get("http://localhost:8008/api/departmentdemands/",{"params":{
                            "userId": this.userId, "departmentId": departmentId, "tenderId": tenderId } })
        .map(res => res.json());
    }
    getTenderDemands(tenderId){
      return this.http.get("http://localhost:8008/api/tenderdemands/",{"params":{
                            "userId": this.userId, "tenderId": tenderId } })
        .map(res => res.json());
    }
    getSingleDemand(demandId){
      return this.http.get("http://localhost:8008/api/demand/",{"params":{
                            "demandId": demandId, "userId": this.userId } })
        .map(res => res.json());
    }
    addDemand(newDemand){
      console.log("new demand called", newDemand);
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http.post("http://localhost:8008/api/adddemand",{
                                      "userId": this.userId,
                                      "year": newDemand.year,
                                      "district": newDemand.district,
                                      "tenderref": newDemand.tender,
                                      "tendername": newDemand.tendername,
                                      "medicine": newDemand.medicine
                                    }, options)
                   .map(res => res.json());
    }
    editDemand(demandId,medicines){
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http.post("http://localhost:8008/api/editdemand",{
                                      "userId": this.userId,
                                      "demandId": demandId,
                                      "medicine": medicines
                                    }, options)
                   .map(res => res.json());
    }
}