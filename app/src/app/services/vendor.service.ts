import {Injectable} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

var userId = localStorage.getItem("userid");

@Injectable()
export class VendorService{
    constructor(private http: Http, 
                private _router: Router){
      console.log('Vendor Service Initialized...');
    }
    getAllVendors(){
      return this.http.get("http://localhost:8008/api/allvendors",
                  {params:{userId:userId}})
                  .map(res => res.json());
    }
    addVendor(newVendor){
        console.log("new vendor called", newVendor);
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        newVendor.userId = userId;
        return this.http.post("http://localhost:8008/api/addvendor", {
                    "userId":newVendor.userId,
                    "name": newVendor.name,
                    "address": newVendor.address,
                    "city": newVendor.city,
                    "phonenumber": newVendor.phonenumber,
                    "email": newVendor.email,
                    "website": newVendor.website,
                    "dateofestablishment": newVendor.dateofestablishment,
                    "businesstype": newVendor.businesstype,
                    "focalpersonname": newVendor.focalpersonname,
                	  "focalpersonnumber": newVendor.focalpersonnumber}, options)
                   .map(res => res.json());
    }
}