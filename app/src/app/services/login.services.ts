import { Injectable, Compiler  } from '@angular/core';
import { Location } from '@angular/common';

import {Http, Headers, RequestOptions} from '@angular/http';
import { Router } from '@angular/router'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class LoginService{
    constructor(private http:Http, private _router: Router, private _compiler: Compiler, private _location: Location){
        console.log('Task Service Initialized...');
    }
    getTasks(username, password){
        console.log("get tasks called");
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        // var body = 'username='+username+'?password='+password+';
        // console.log("BODY PARAMETER: ", body);
        return this.http.post('http://localhost:8008/api/auth/login', {"username": username, "password": password}, options)
            .map(res => res.json());
    }
    getAuthUser(userId){
        console.log("USER ID FROM AUTH USER: ", userId);
        return this.http.get('http://localhost:8008/api/user/'+userId)
            .map(res => res.json());
    }
    loggedIn(){
        if (localStorage.getItem("userid") === null){
          return false
        }
        return true
    }
    logout() {
        // this._compiler.clearCache();
        localStorage.removeItem("userid");
        localStorage.removeItem("userrole");
        this._router.navigate(['']);
        window.location.reload();

    }
}