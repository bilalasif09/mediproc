const DemandModel = model('demand');
const UserModel = model('user');
const DemandHistoryModel = model('demandhistory');

exports.dddemands = function (req, res, next) {

	UserModel.findById(req.query.userId, function (err, user) {
		if (err) {
			res.json(err);
		}
		console.log("Dep",req.body.departmentId)
		if(user.role==='user' || user.role==='pco'){
			DemandModel.find(
				{
					department: req.query.departmentId,
					tenderref: req.query.tenderId,
					district: req.query.district
				}
				,function(err, demands) {
				if (err) {
					res.json(err);
				}
				res.json([{status:1,message:'Access Granted',demands: demands}]);
			});
		}
		else {
			res.json([{status:0,message:'Access Denied',demands: []}]);
		}
	})
}
exports.departmentdemands = function (req, res, next) {

	UserModel.findById(req.query.userId, function (err, user) {
		if (err) {
			res.json(err);
		}
		console.log("Dep",req.body.departmentId)
		if(user.role==='user' || user.role==='pco'){
			DemandModel.find(
				{
					department: req.query.departmentId,
					tenderref: req.query.tenderId
				}
				,function(err, demands) {
				if (err) {
					res.json(err);
				}
				res.json([{status:1,message:'Access Granted',demands: demands}]);
			});
		}
		else {
			res.json([{status:0,message:'Access Denied',demands: []}]);
		}
	})
}
exports.tenderdemands = function (req, res, next) {

	UserModel.findById(req.query.userId, function (err, user) {
		if (err) {
			res.json(err);
		}
		if(user.role==='user' || user.role==='pco'){
			DemandModel.find(
				{
					tenderref: req.query.tenderId
				}
				,function(err, demands) {
				if (err) {
					res.json(err);
				}
				res.json([{status:1,message:'Access Granted',demands: demands}]);
			});
		}
		else {
			res.json([{status:0,message:'Access Denied',demands: []}]);
		}
	})
}
exports.alldemands = function (req, res, next) {

	UserModel.findById(req.query.userId, function (err, user) {
		if (err) {
			res.json(err);
		}
		if(user.role==='user' || user.role==='pco' || user.role==='admin'){
			DemandModel.find(function(err, demands) {
				if (err) {
					res.json(err);
				}
				res.json([{status:1,message:'Access Granted',demands: demands}]);
			});
		}
		else {
			res.json([{status:0,message:'Access Denied',demands: []}]);
		}
	})
}
exports.demand = function (req, res, next) {
	UserModel.findById(req.query.userId, function (err, user) {
		if (err){
			res.json(err);
		}
		if(user.role==='user' || user.role==='pco' || user.role==='admin'){
			DemandModel.findById(req.query.demandId, function (err, demand) {
			  	if (err) {
					res.json(err);
				}
				res.json([{status:1,message:'Access Granted',demand:demand}]);
			});
		}
		else {
			res.json([{status:0,message:'Access Denied',demand:null}]);
		}
	})
}
exports.adddemand = function (req, res, next) {
	
	UserModel.findById(req.body.userId, function (err, user) {
		
		if (err){
			res.json(err);
		}
		if(user.role==='user' || user.role==='pco'){
			var saveDemand = model('demand');
			var newDemand = new saveDemand({
									year: req.body.year,
									district: req.body.district,
									department: user.department[0]._id,
									departmentname: user.department[0].name,
									demandstatus: 'pending',
									user: req.body.userId,
									tenderref: req.body.tenderref,
									tendername: req.body.tendername,
									medicine: req.body.medicine
								});
		}
		if(user.role==='user' || user.role==='pco' || user.role==='admin'){
			newDemand.save(function(err, demand) {
				if (err) {
					res.send(err);
				}
				res.json([{status:1,message:'Demand Created',demand:demand}]);
			});
		}
		else{
			res.json([{status:0,message:'Access Denied',demand:null}]);
		}
	})
}
exports.editdemand = function (req, res, next) {
	
	UserModel.findById(req.body.userId, function (err, user) {
		
		if (err){
			res.json(err);
		}
		console.log("Med->",req.body.medicine,req.body.demandId)
		if(user.role==='user' || user.role==='pco'){
			DemandModel.findById(req.body.demandId, function(err, demand) {
				if (err){
					res.json(err);
				} else {
					req.body.medicine.forEach(function (value, i) {
					    console.log('%d: %s', i, value);
					    demand.medicine[i].quantity = value.newquantity;
					    demand.medicine[i].reason = value.newreason;
					});
					var saveDemandHistory = model('demandhistory');
					var newDemandHistory = new saveDemandHistory({
											user: user._id,
											demand: demand._id,
											medicine: req.body.medicine
										});


					console.log("Demand After saving->",demand,"BODY",req.body.medicine)
					demand.save((err, newdemand) => {
						if (err) res.send(err)
						newDemandHistory.save((err, demand) => {
							if (err) res.send(err)
							res.json([{status:1,message:'Demand Updated',demand:
								{olddemand: demand, newdemand: newdemand }

							}]);
						})
					})
				}
			})
		}
		else{
			res.json([{status:0,message:'Access Denied',demand:null}]);
		}
	})
}
exports.updatedeptdemandstate = function (req, res, next) {
	
	UserModel.findById(req.body.userId, function (err, user) {
		
		if (err){
			res.json(err);
		}
		if(user.role==='user' || user.role==='pco'){
			DemandModel.find(
					{ 	"tenderref": req.body.tenderId,
						"department": req.body.deptId
					}, function(err, demands) {
				if (err){
					res.json(err);
				} else {
					demands.forEach(function (value, i) {
					    demands[i].demandstatus = req.body.demState.toString();
						demands[i].save((err,newdemands) => {
							if (err) res.send(err)
						});
					});
					res.json([{status:1,message:'Demands Updated',demands:[]}]);
				}
			})
		}
		else{
			res.json([{status:0,message:'Access Denied',demand:null}]);
		}
	})
}
exports.updatedistrictdemandstate = function (req, res, next) {
	
	UserModel.findById(req.body.userId, function (err, user) {
		
		if (err){
			res.json(err);
		}
		if(user.role==='user' || user.role==='pco'){
			DemandModel.find(
					{ 	"tenderref": req.body.tenderId,
						"department": req.body.deptId,
						"district": req.body.district
					}, function(err, demands) {
				if (err){
					res.json(err);
				} else {
					demands.forEach(function (value, i) {
					    demands[i].demandstatus = req.body.demState.toString();
						demands[i].save((err,newdemands) => {
							if (err) res.send(err)
						});
					});
					res.json([{status:1,message:'Demands Updated',demands:[]}]);
				}
			})
		}
		else{
			res.json([{status:0,message:'Access Denied',demand:null}]);
		}
	})
}
exports.updatedemandstate = function (req, res, next) {
	
	UserModel.findById(req.body.userId, function (err, user) {
		
		if (err){
			res.json(err);
		}
		if(user.role==='user' || user.role==='pco'){
			DemandModel.findById(req.body.demandId, function(err, demand) {
				if (err){
					res.json(err);
				} else {
					demand.demandstatus = req.body.demState.toString();
					demand.save((err,demand) => {
						if (err) res.send(err)
					});
					res.json([{status:1,message:'Demands Updated',demand: demand}]);
				}
			})
		}
		else{
			res.json([{status:0,message:'Access Denied',demand:null}]);
		}
	})
}

exports.updatetenderdemandstate = function (req, res, next) {
	
	UserModel.findById(req.body.userId, function (err, user) {
		
		if (err){
			res.json(err);
		}
		if(user.role==='user' || user.role==='pco'){
			DemandModel.find({"tenderref":req.body.tenderId}, function(err, demands) {
				if (err){
					res.json(err);
				} else {
					demands.forEach(function (value, i) {
					    demands[i].demandstatus = req.body.demState.toString();
						demands[i].save((err,newdemands) => {
							if (err) res.send(err)
						});
					});
					res.json([{status:1,message:'Demands Updated',demands:[]}]);
				}
			})
		}
		else{
			res.json([{status:0,message:'Access Denied',demand:null}]);
		}
	})
}