var controller = require('./vendor.ctrl.js')

module.exports = function(router){
    router.get('/allvendors', controller.allvendors )
    router.post('/addvendor', controller.addvendor )
}