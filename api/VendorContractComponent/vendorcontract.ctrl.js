const VendorContractModel = model('vendorcontract');
const UserModel = model('user');
var mongoose = require('mongoose');

exports.addVendorContract = function(req, res, next){
  // res.send("ALL VENDOR PRE QUALIFICATIONS");
  UserModel.findById(req.body.userId, function (err, user) {
    if (err) {
      res.json(err);
    }
    if(user.role==='pco' || user.role==='deo' || user.role==='admin'){
      // VendorPreQualiModel.find(function(err, profiles){
      //   if(err) {
      //     res.json(err)
      //   }
      //   res.json([{status:1,message:'Access Granted',profiles:profiles}]);
      // });
      var newContract = new VendorContractModel({
        tenderId: req.body.tenderId,
        vendorId: req.body.vendorId,
        contractprofileId: req.body.contractprofileId,
        contractprofiletext: req.body.contractprofiletext,
        contractdate: req.body.contractdate
      });
      newContract.save(function(err, result){
        if(err) res.send(err)
        res.json([{status:1,message:'Access Granted',result:result}]);
      });
      // res.json("add vendor pre qualification");
    }
    else {
      res.json([{status:0,message:'Access Denied',result:[]}]);
    }
  });
}
exports.allVendorContracts = function(req, res, next){
  UserModel.findById(req.query.userId, function (err, user) {
    if (err) {
      res.json(err);
    }
    if(user.role==='pco' || user.role==='deo' || user.role==='admin'){
      VendorContractModel.find(function(err, contracts){
        if(err) {
          res.json(err)
        }
        res.json([{status:1,message:'Access Granted',contracts:contracts}]);
      });
    }
    else {
      res.json([{status:0,message:'Access Denied',contracts:[]}]);
    }
  });
}
