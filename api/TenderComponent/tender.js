var controller = require('./tender.ctrl.js')

module.exports = function(router){
    router.get('/alltenders', controller.alltenders )
    router.get('/tendersbydepartment', controller.tendersbydepartment )
    router.post('/addtender', controller.addtender )
    router.get('/locktender', controller.locktender )
    router.get('/edittender/:tenderId', controller.editTender )
    router.get('/gettender', controller.getTender )
    router.get('/tenderqualiprofiles', controller.getQualiProfiles )
}